import labescape.LabEscape;
import labescape.NoEscapeException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

import static org.junit.Assert.assertArrayEquals;

public class LabEscapeTest {
    private LabEscape labEscape;

    @Before
    public void init() {
        labEscape = new LabEscape();
    }


    @Test
    public void shouldNotFindAWay() {
        //Given
        final char[][] maze = readMaze("noWay.txt");

        //When
        final char[][] solution;
        try {
            labEscape.drawPathForEscape(maze, 1, 1);
            Assert.fail("Should not find a solution");
        } catch (NoEscapeException ignored) {
            //Then
        }

    }

    @Test
    public void shouldFindOnePossibleWay() throws NoEscapeException {
        //Given
        final char[][] maze = readMaze("4x4.txt");
        final char[][] correctSolution = readMaze("4x4Solution.txt");

        //When
        final char[][] solution = labEscape.drawPathForEscape(maze, 1, 1);

        //Then
        printMaze(solution);
        printMaze(correctSolution);
        assertArrayEquals(correctSolution, solution);
    }

    @Test
    public void shouldFindShortestWay() throws NoEscapeException {
        //Given
        final char[][] maze = readMaze("oneWay.txt");
        final char[][] correctSolution = readMaze("oneWaySolution.txt");

        //When
        final char[][] solution = labEscape.drawPathForEscape(maze, 3, 1);

        //Then
        printMaze(solution);
        printMaze(correctSolution);
        assertArrayEquals(correctSolution, solution);
    }

    @Test
    public void shouldFindShortestWayInTwoPossible() throws NoEscapeException {
        //Given
        final char[][] maze = readMaze("twoWays.txt");
        final char[][] correctSolution = readMaze("twoWaysSolution.txt");

        //When
        final char[][] solution = labEscape.drawPathForEscape(maze, 3, 1);

        //Then
        printMaze(solution);
        printMaze(correctSolution);
        assertArrayEquals(correctSolution, solution);
    }

    private void printMaze(char[][] maze) {
        for (int i = 0; i < maze.length; i++) {
            char[] row = maze[i];
            for (int j = 0; j < row.length; j++) {
                System.out.print(maze[i][j]);
            }
            System.out.println();
        }
    }

    private char[][] readMaze(String pathToFile) {

        final Path path = Paths.get(pathToFile);

        final ArrayList<String> lines = lines(path.toString());
        final int rows = lines.size();
        final int columns = lines.get(0).toCharArray().length;
        final char[][] result = new char[rows][columns];
        for (int i = 0; i < rows; i++) {
            Arrays.fill(result[i], ' ');
            char[] chars = lines.get(i).toCharArray();
            for (int j = 0; j < chars.length; j++) {
                result[i][j] = chars[j];
            }
        }
        return result;

    }

    private ArrayList<String> lines(String path) {
        final ArrayList<String> lines = new ArrayList<>();
        try (Scanner scanner = new Scanner(this.getClass().getResourceAsStream(path))) {
            while (scanner.hasNextLine()) {
                lines.add(scanner.nextLine());
            }
        }
        return lines;

    }
}