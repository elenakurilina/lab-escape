package web;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Arrays;

public class MazeRequest {

    @Min(0)
    private int startX;
    @Min(0)
    private int startY;
    @NotNull
    @NotEmpty
    private String maze;

    public MazeRequest() {
    }

    public MazeRequest(int startX, int startY, String maze) {
        this.startX = startX;
        this.startY = startY;
        this.maze = maze;
    }

    public int getStartX() {
        return startX;
    }

    public int getStartY() {
        return startY;
    }

    public String getMaze() {
        return maze;
    }

    public void setStartX(int startX) {
        this.startX = startX;
    }

    public void setStartY(int startY) {
        this.startY = startY;
    }

    public void setMaze(String maze) {
        this.maze = maze;
    }

    public char[][] convertToArray() {
        String[] lines = maze.split("\n");
        final int columns = lines[0].toCharArray().length;
        final int rows = lines.length;
        final char[][] maze = new char[rows][columns];
        for (int i = 0; i < rows; i++) {
            Arrays.fill(maze[i], ' ');
            char[] chars = lines[i].toCharArray();
            System.arraycopy(chars, 0, maze[i], 0, chars.length);
        }
        return maze;
    }

}
