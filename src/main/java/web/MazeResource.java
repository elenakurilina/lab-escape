package web;


import labescape.LabEscape;
import labescape.NoEscapeException;

import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/maze")
public class MazeResource {

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public MazeResponse register(@Valid MazeRequest mazeRequest) {
        try {
            char[][] solution = LabEscape.drawPathForEscape(mazeRequest.convertToArray(), mazeRequest.getStartX(), mazeRequest.getStartY());
            return MazeResponse.build(solution);
        } catch (NoEscapeException e) {
            return MazeResponse.noSolution();
        }

    }


}
