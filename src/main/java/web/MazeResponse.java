package web;

public class MazeResponse {

    private String solution;

    public MazeResponse() {
    }

    public static MazeResponse noSolution() {
        return new MazeResponse("No solution was found");
    }

    public static MazeResponse build(char[][] solution) {
        StringBuilder builder = new StringBuilder();
        for (char[] chars : solution) {
            builder.append(new String(chars));
            builder.append(System.getProperty("line.separator"));
        }
        return new MazeResponse(builder.toString());
    }

    public MazeResponse(String solution) {
        this.solution = solution;
    }

    public String getSolution() {
        return solution;
    }

    public void setSolution(String solution) {
        this.solution = solution;
    }
}
