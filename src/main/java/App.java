import com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException;
import io.dropwizard.Application;
import io.dropwizard.setup.Environment;
import web.MazeResource;

public class App extends Application<AppConfiguration> {

    @Override
    public void run(AppConfiguration appConfiguration, Environment environment) throws Exception {
        environment.jersey().register(new MazeResource());
    }

    public static void main(String[] args) throws Exception {
        try {
            new App().run(args);
        } catch (UnrecognizedPropertyException e) {
            System.exit(-1);
        }
    }
}
