package labescape;

import java.util.stream.Collectors;
import java.util.*;

public class Node {
    public final int row;
    public final int column;
    private final Set<Node> neighbours;

    private Node predecessor = null;
    private int distanceToTheFirstNode = Integer.MAX_VALUE;

    public Node(int row, int column) {
        this.row = row;
        this.column = column;
        this.neighbours = new HashSet<>();
    }

    public void addNeighbour(Node... nodes) {
        this.neighbours.addAll(Arrays.stream(nodes).collect(Collectors.toSet()));
    }

    public ArrayList<Node> findShortestWayTo(Node to) {
        final Set<Node> unsettled = new HashSet<>();
        final Set<Node> settled = new HashSet<>();

        this.distanceToTheFirstNode = 0;
        unsettled.add(this);
        while (!unsettled.isEmpty()) {

            final Node evaluated = findClosest(unsettled);
            evaluated.neighbours.forEach(neighbour -> evaluated.findUnsettledNeighboursWithOptimalDistance(unsettled, settled, neighbour));
            unsettled.remove(evaluated);
            settled.add(evaluated);

            if (evaluated.equals(to)) {
                return findShortestPath(to);
            }

        }
        return new ArrayList<>();
    }

    private ArrayList<Node> findShortestPath(Node to) {
        final ArrayList<Node> path = new ArrayList<>();
        Node current = to;
        while (!current.equals(this)) {
            path.add(current);
            current = current.predecessor;
        }
        path.add(current);
        return path;
    }

    private void findUnsettledNeighboursWithOptimalDistance(Set<Node> unsettled, Set<Node> settled, Node n) {
        if (!unsettled.contains(n) && !settled.contains(n)) {
            if (this.distanceToTheFirstNode + 1 < n.distanceToTheFirstNode) {
                n.distanceToTheFirstNode = this.distanceToTheFirstNode + 1;
                n.predecessor = this;
            }
            unsettled.add(n);
        }
    }

    private static Node findClosest(Collection<Node> unsettled) {
        return unsettled.stream().sorted((o1, o2) -> o1.distanceToTheFirstNode - o2.distanceToTheFirstNode).findFirst().get();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Node node = (Node) o;

        if (row != node.row) return false;
        return column == node.column;

    }

    @Override
    public int hashCode() {
        int result = row;
        result = 31 * result + column;
        return result;
    }
}