package labescape;

import javafx.util.Pair;

import java.util.*;

/**
 * Please implement your solution here
 */
public class LabEscape {

    public static final char WALL = 'O';
    public static final char FREE = ' ';
    public static final char PATH = '•';

    /**
     * @param labyrinth A labyrinth drawn on a matrix of characters. It's at least 4x4, can be a rectangle or a square.
     *                  Walkable areas are represented with a space character, walls are represented with a big O character.
     *                  The escape point is always on the border (see README)
     * @param startX    Starting row number for the escape. 0 based.
     * @param startY    Starting column number for the escape. 0 based.
     * @return A char matrix with the same labyrinth and a path drawn from the starting point to the escape
     * @throws NoEscapeException when no path exists to the outside, from the selected starting point
     */
    public static char[][] drawPathForEscape(char[][] labyrinth, int startX, int startY) throws NoEscapeException {
        final Pair<Node, Set<Node>> nodeSetPair = NodeConverter.convertToNodes(labyrinth, startX, startY);

        final Node start = nodeSetPair.getKey();
        if (start == null) {
            throw new NoEscapeException();
        }
        final Collection<Node> exits = nodeSetPair.getValue();

        Optional<ArrayList<Node>> escape = exits.stream()
                .map(start::findShortestWayTo)
                .sorted(Comparator.comparing(Collection::size))
                .findFirst();
        if (escape.isPresent() && !escape.get().isEmpty()) {
            escape.get().forEach(node -> labyrinth[node.row][node.column] = PATH);
        } else {
            throw new NoEscapeException();
        }
        return labyrinth;

    }


}
