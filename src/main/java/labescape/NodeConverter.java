package labescape;

import javafx.util.Pair;

import java.util.HashSet;
import java.util.Set;


public class NodeConverter {

    public static Pair<Node, Set<Node>> convertToNodes(char[][] labyrinth, int startX, int startY) {
        Node first = null;
        Node start = null;
        final Set<Node> exits = new HashSet<>();
        final Node[][] nodes = createAllNodes(labyrinth);

        for (int i = 0; i < labyrinth.length; i++) {
            char[] rows = labyrinth[i];
            for (int j = 0; j < rows.length; j++) {
                char cell = rows[j];
                Node node = nodes[i][j];
                if (cell == LabEscape.FREE) {
                    if (first == null) {
                        first = node;
                    } else {
                        addNeighbour(labyrinth, i - 1, j, node, nodes);
                        addNeighbour(labyrinth, i + 1, j, node, nodes);
                        addNeighbour(labyrinth, i, j - 1, node, nodes);
                        addNeighbour(labyrinth, i, j + 1, node, nodes);
                    }
                    if (i == 0 || i == labyrinth.length - 1 || j == 0 || j == rows.length - 1) {
                        exits.add(node);
                    }
                    if (i == startX && j == startY) {
                        start = node;
                    }
                }


            }

        }
        return new Pair<>(start, exits);
    }

    private static Node[][] createAllNodes(char[][] labyrinth) {
        final Node[][] nodes = new Node[labyrinth.length][labyrinth[0].length];
        for (int i = 0; i < labyrinth.length; i++) {
            char[] rows = labyrinth[i];
            for (int j = 0; j < rows.length; j++) {
                nodes[i][j] = new Node(i, j);
            }
        }
        return nodes;
    }

    private static void addNeighbour(char[][] labyrinth, int i, int j, Node node, Node[][] nodes) {
        if (
                i >= 0
                        && j >= 0
                        && i < labyrinth.length
                        && j < labyrinth[0].length
                        && labyrinth[i][j] == LabEscape.FREE) {
            nodes[i][j].addNeighbour(node);
            node.addNeighbour(nodes[i][j]);
        }
    }
}
